#include <iostream>
#include <conio.h>
#include <string>

using namespace std;


enum Rank
{
	Two = 2,
	Three,
	Four,
	Five,
	Six,
	Seven,
	Eight,
	Nine,
	Ten,
	Jack,
	Queen,
	King,
	Ace
};

enum Suit
{
	Spades,
	Clubs,
	Hearts,
	Diamonds
};

struct Card
{
	Suit Suit;
	Rank Rank;
};

void PrintCard(Card card) {
	string strRank;
	string strSuit;

	switch (card.Rank) {
		case Two:strRank = "Two"; break;
		case Three:strRank = "Three"; break;
		case Four:strRank = "Four"; break;
		case Five:strRank = "Five"; break;
		case Six:strRank = "Six"; break;
		case Seven:strRank = "Seven"; break;
		case Eight:strRank = "Eight"; break;
		case Nine:strRank = "Nine"; break;
		case Ten:strRank = "Ten"; break;
		case Jack:strRank = "Jack"; break;
		case Queen:strRank = "Queen"; break;
		case King:strRank = "King"; break;
		case Ace:strRank = "Ace"; break;
	};

	switch (card.Suit) {
		case Spades:strSuit = "Spades"; break;
		case Clubs:strSuit = "Clubs"; break;
		case Hearts:strSuit = "Hearts"; break;
		case Diamonds:strSuit = "Diamonds"; break;
	};

	cout << "The " + strRank + " of " + strSuit + ".\n";

}

Card HighCard(Card card1, Card card2) {
	if (card1.Rank > card2.Rank) {
		return card1;
	}
	else if (card1.Rank < card2.Rank) {
		return card2;
	}
	else {
		//same rank

		//return null?
	}
}

int main()
{
	//Test print card
	Card c1;
	c1.Rank = Two;
	c1.Suit = Hearts;

	Card c2;
	c2.Rank = Three;
	c2.Suit = Spades;

	PrintCard(c1);
	PrintCard(HighCard(c1,c2));

	_getch();
	return 0;
}

